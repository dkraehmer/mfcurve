v 1.0
d MFCURVE: A Stata package for plotting results from multifactorial research designs.
d  
d Distribution-Date: 20230613
d
d Daniel Krähmer
d daniel.kraehmer@soziologie.uni-muenchen.de