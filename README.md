# mfcurve

Stata ado for plotting results from multifacotiral research designs (i.e. conjoint analyses, choice experiments, factorial survey experiments). 

## Installation

The ado is currently only available via GitLab. A revised version is expected to be published on SSC soon. 

```
net install mfcurve, from("https://gitlab.lrz.de/dkraehmer/mfcurve/-/raw/main/installation") replace
```


## Syntax

The syntax for ``mfcurve`` is as follows:

```
mfcurve varname, factors(varlist) 
    [   groupvar(varname) test(mean|zero) level(#) show(show_options) boxplot                       
        style_m_sig(marker_options) 
        style_m_nosig(marker_options) 
        style_ci_sig(line_options) 
        style_ci_nosig(line_options)
        style_ind_act(marker_options)
        style_ind_pas(marker_options)
        style_l_mean(line_options)    
        twoway_options 
    ]
```

With show_options being ``mean``, ``sig``, ``ci_regular``, ``ci_gradient``, and `` groupsize``. For more details, see ``mfcurve.sthlp``.

## Description
``mfcurve`` plots the mean of variable ***varname*** over all multifactorial combinations in **factors(*varlist*)**.

The plot consists of two subgraphs: an upper panel, displaying the mean outcome per group; and a lower panel, indicating the presence/absence of each factor level. Symbols in the upper panel are referred to as markers; symbols in the lower panel are referred to as indicators.  The graph follows the overall aesthetics of specification curve but displays effect variation across *treatment specifications* instead of effect variation across *model specifications*.

## Examples

Load Stata's system dataset nlsw88.dta, drop missing observations, and plot the average income across groups defined by variables race, region, and union membership:

```
sysuse nlsw88, clear
drop if missing(race, south, union, wage)
mfcurve wage, factors(race south union)
```

<div align="center">
![Basic example of mfcurve](/mfcurve_examples/mfcurve_basic.png "mfcurve_basic")
</div>

In the graph, higher hourly wages seem to be associated with union membership and not living in the south. To assess if this is due to small cell sizes in certain subgroups, we can type:


```
mfcurve wage, factors(race south union) show(groupsize)
```

<div align="center">
![Example of mfcurve using option groupsize](/mfcurve_examples/mfcurve_groupsize.png "mfcurve_groupsize")
</div>

Indeed, there are very few observations with race == "other" (groups 9-11), so we might want to drop these. We then test if the income of each remaining group differs significantly (at the 99% level) from the mean income across all other groups:

```
drop if race == 3
mfcurve wage, factors(race south union) show(sig) test(mean) level(99)
```

<div align="center">
![Example of mfcurve using option show(sig)](/mfcurve_examples/mfcurve_sig.png "mfcurve_sig")
</div>


We may obtain a full-fledged ``mfcurve`` by relabeling/renaming variables, displaying the overall mean and confidence intervals, removing the automatically generated (and here: meaningless) group identifiers on the x-axis, customizing the marker symbols, and specifying a few general twoway_options:

```
rename race Race
rename south Region
label define region_lbl 0"North" 1"South"
label values Region region_lbl
rename union Union
label define union_lbl 0"No" 1"Yes"
label values Union union_lbl

local twoptions xlab(none) ylab(5(1)11) ytitle("Hourly Wage (in US$)") xtitle("") graphregion(color(white))
mfcurve wage, factors(Race Region Union) show(sig mean ci_gradient) test(mean) level(99) style_m_sig(msymbol(D)) `twoptions'
```

<div align="center">
![Advanced example of mfcurve using various options](/mfcurve_examples/mfcurve_advanced.png "mfcurve_advanced")
</div>

## Acknowledgment and Author Details
This ado has drawn inspiration from the Stata ado ``speccurve``, authored by Hans H. Sievertsen. I am grateful to my colleagues at the Department of Sociology at LMU Munich for their helpful comments and suggestions.

**Please feel free to reach out for any suggestions and improvements!** 

Daniel Krähmer, Ludwig-Maximilans-Universität (LMU) Munich

Contact: daniel.kraehmer@soziologie.uni-muenchen.de
